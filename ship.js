class Ship extends Game {
    title = "Ship Agenixo I";
    width = 10;
    height = 10;
    block = 30;
    interval = 1000;

    ship;
    treasure;
    rocks = [];
    tornadoes = [];
    score = 0;
    commands = [];


    spriteImage(image) {
        return '<img src=" ' + image + '" style="width: 30px; height: 30px; margin: 0; padding: 0;" />';
    }
    
    cssTable() {
        return "background-image: url('tiles/water.png');";
    }

    cssCells() {
        return "text-align: center;";
    }

    onRender() {
        this.addTopInfo("Tour", "0");
        this.addTopInfo("Score", this.score);
    }

    onStart() {
        this.commands = document.getElementById('input').value.split("\n");
        this.onRender();
    }

    onBuild(x, y, cell) {
        if(cell == 'S') {
            this.ship = this.addSprite(this.spriteImage("tiles/boat.png"), x, y, true);
            this.ship.tornadoes = 0;
        } else if(cell == '#') {
            this.rocks.push(this.addSprite(this.spriteImage("tiles/rock.png"), x, y, true));
        } else if(cell == 'T') {
            this.treasure = this.addSprite(this.spriteImage("tiles/treasure.png"), x, y, true);
        } else if(cell == 'W') {
            this.tornadoes.push(this.addSprite(this.spriteImage("tiles/tornado.png"), x, y, true));
        }
    }

    onCollision(a, b) {
        console.log(a, b);
        if(this.tornadoes.some(tornado => tornado.is(a))) {
            this.deleteSprite(a, false);
            this.ship.tornadoes = 3;
        }
        if(this.treasure.is(a)) {
            this.deleteSprite(a, false);
            this.deleteSprite(b, false);
            this.stop();
        }
        if(this.rocks.some(rock => rock.is(a))) {
            this.deleteSprite(a);
            this.deleteSprite(b);
            let explosion = this.addSprite(this.spriteImage("tiles/explosion.png"), a.x, a.y, false);
            explosion.display = true;
            this.stop();
        }
    }

    onStop() {
        this.addTopInfo("Score", "GAME OVER");
    }

    onLoop(tick) {

        let command = this.commands[tick];

        console.log(tick);
        console.log(command);

        let step = 1;
        
        if(0 < this.ship.tornadoes) {
            step = 3;
            this.ship.tornadoes--;
        }

        if(command == 'UP') this.ship.moveUp(step);
        else if(command == 'DOWN') this.ship.moveDown(step);
        else if(command == 'LEFT') this.ship.moveLeft(step);
        else if(command == 'RIGHT') this.ship.moveRight(step);
        else this.stop();

    }
    
}