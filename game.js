class Game {

    title = "";

    idSprite = 1;
    width;
    height;
    map;
    block;
    interval = 1000;

    sprites = [];
    bindedKeys = [];

    loop = null;

    topInfos = [];
    bottomInfos = [];

    onRender() {
    }

    onBuild(cell) {
    }

    onCollision(x, y) {
    }

    onStart() {
    }

    onStop() {
    }

    onLoop() {
    }

    onCollision(a, b) {
    }

    build(map) {
        let lines = map.split("\n");
        this.height = lines.length;
        for(let i = 0; i < this.height; i++) {
            let line = lines[i];
            let cells = line.split('');
            this.width = cells.length;
            for(let j = 0; j < this.width; j++) {
                let cell = cells[j];
                this.onBuild(cell);
            }
        }
        return this;
    }

    autoDefineWidthHeight(map) {
        let lines = this.map.split("\n");
        this.height = lines.length;
        this.width = lines[0].split('').length;
        return this;
    }

    autoBuildMap() {
        let lines = this.map.split("\n");
        for(let i = 0; i < this.height; i++) {
            let line = lines[i];
            let cells = line.split('');
            for(let j = 0; j < this.width; j++) {
                this.onBuild(i, j, cells[j]);
            }
        }
        return this;
    }

    render(id, map) {
        let element = document.getElementById(id);
        this.map = map;
        this.autoDefineWidthHeight();
        element.innerHTML = this.renderTop();
        element.innerHTML += this.renderGrid();
        element.innerHTML += this.renderBottom();
        this.autoBuildMap();
        document.getElementById('start').onclick = () => this.start();
        document.getElementById('stop').onclick = () => this.stop();
        document.getElementById('start').disabled  = false;
        document.getElementById('stop').disabled  = true;
        this.onRender();
        this.refreshInfos();
        return this;
    }

    reset() {
        this.sprites = [];
        for(let y = 0; y < this.height; y++) {
            for(let x = 0; x < this.width; x++) {
                this.setCell(x, y, '');
            }
        }
        return this;
    }

    start() {
        this.reset();
        this.autoBuildMap(map);
        document.getElementById('start').disabled  = true;
        document.getElementById('stop').disabled  = false;
        this.enableKeyBoard();
        let tick = 0;
        this.loop = setInterval(() => { 
            this.refreshInfos(); 
            this.onLoop(tick++); 
            this.refreshInfos(); 
        }, this.interval);
        this.onStart();
        return this;
    }

    stop() {
        document.getElementById('stop').disabled  = true;
        document.getElementById('start').disabled  = false;
        this.disableKeyBoard();
        if(this.loop) clearInterval(this.loop);
        this.onStop();
        this.refreshInfos();
        return this;
    }

    enableKeyBoard() {

        window.onkeypress = (event) => {
            
            if(typeof this.bindedKeys[event.keyCode] === 'undefined')
                return;
                
            return this.bindedKeys[event.keyCode]();
            
        };

        return this;

    }

    disableKeyBoard() {

        window.onkeypress = null;

        return this;

    }

    bindKey(key, callback) {
        this.bindedKeys[key] = callback;
        return this;
    }
    
    bindKeyLeft(callback) { this.bindKey(37, callback); this.bindKey(52, callback); }
    bindKeyUp(callback) { this.bindKey(38, callback); this.bindKey(56, callback); }
    bindKeyRight(callback) { this.bindKey(39, callback); this.bindKey(54, callback); }
    bindKeyDown(callback) { this.bindKey(40, callback); this.bindKey(50, callback); }
    bindKeySpace(callback) { this.bindKey(32, callback); }

    renderTop() {
        let html = '<div id="top">';
        html += '<h3 id="title">' + this.title + '</h3>';
        html += '<p>Width: ' + this.width + ', Height: ' + this.height + ', Block: ' + this.block + '</p>';
        html += '<p>';
        html += '<input id="start" type="button" value="Start" />';
        html += '<input id="stop" type="button" value="Stop" />';
        html += '</p>';
        html += '<p id="topInfos"></p>';
        html += '</div>';
        return html;
    }

    cssTable() {
        return '';
    }

    cssCells() {
        return '';
    }

    renderGrid() {

        let html = '<div style="display: flex; flex-direction: row; justify-content: center">';
        html += '<div>';
        html += '<div id="top-numbers" style="display: flex; flex-direction: row; justify-content: flex-start">';
        for(let x = -1 ; x < this.width; x++) {
            html += '<p style="width:'+this.block+'px;height:'+this.block+'px; margin: 0; padding: 0; line-height: ' + this.block + 'px;' + this.cssCells() + '">' + ( x >= 0 ? x : '') + '</p>'
        }
        html += '</div>';
        html += '<div style="display: flex; flex-direction: row; justify-content: flex-start">';
        html += '<div style="display: flex; flex-direction: column; justify-content: flex-start">';
        for(let y = 0; y < this.height; y++) {
            html += '<p style="width:'+this.block+'px;height:'+this.block+'px; margin: 0; padding: 0; line-height: ' + this.block + 'px;' + this.cssCells() + '">' + y + '</p>'
        }
        html += '</div>';
        html += '<table style="border-collapse: collapse; margin: 0; padding: 0; ' + this.cssTable() + '">';
        for(let y = 0; y < this.height; y++) {
            html += '<tr id="' + this.getRowId(y) + '">';
            for(let x = 0; x < this.width; x++) {
                html += '<td id="' + this.getCellId(x, y) + '" style="width:'+this.block+'px;height:'+this.block+'px; margin: 0; padding: 0; line-height:10px; ' + this.cssCells() + '"></td>';
            }
            html += '</tr>';
        }
        html += '</table>';
        html += '</div>';
        html += '</div>';
        return html;
    }

    renderBottom() {
        let html = '<div id="bottom">';
        html += '<p id="bottomInfos"></p>';
        html += '</div>';
        return html;
    }

    addBottomInfo(label, value) {
        this.bottomInfos[label] = value;
        return this;
    }

    addTopInfo(label, value) {
        this.topInfos[label] = value;
        return this;
    }

    refreshInfos() {

        this.refreshTopInfos();
        this.refreshBottomInfos();
        
        return this;
        
    }

    refreshTopInfos() {

        let topInfos = document.getElementById('topInfos');

        topInfos.innerHTML = '';

        for(let label in this.topInfos)
            topInfos.innerHTML += label + ': ' + this.topInfos[label] + '<br />';

        return this;

    }

    refreshBottomInfos() {

        let bottomInfos = document.getElementById('bottomInfos');

        bottomInfos.innerHTML = '';

        for(let label in this.bottomInfos)
            bottomInfos.innerHTML += label + ': ' + this.bottomInfos[label];

        return this;

    }

    getRowId(y) {
        return 'r' + y;
    }

    getCellId(x, y) {
        return this.getRowId(y) + 'c' + x;
    }

    getCell(x, y) {
        return document.getElementById(this.getCellId(x, y));
    }

    getCellSprites(x, y) {
        return this.sprites.filter((sprite) => {
            return sprite.x == x && sprite.y == y;
        });
    }

    getRandomX() {
        return Math.floor(Math.random()*(this.width - 1));
    }

    getRandomY() {
        return Math.floor(Math.random()*(this.height - 1));
    }

    setCell(x, y, html) {
        this.getCell(x, y).innerHTML = html;
        return this;
    }

    appendCell(x, y, html) {
        this.getCell(x, y).innerHTML += html;
        return this;
    }

    refresh() {

        for(let y = 0; y < this.height; y++) 
            this.refreshRow(y);

        return this;

    }

    refreshRow(y) {

        for(let x = 0; x < this.width; x++)
            this.refreshCell(x, y);

        return this;

    }

    refreshCell(x, y) {
        this.setCell(x, y, '');
        let collisions = this.getCellSprites(x, y);
        while(1 < collisions.length) {
            let collision = collisions.pop();
            collisions.map(sprite => this.onCollision(collision, sprite));
        }
        this.getCellSprites(x, y).map((sprite) => {
            if(sprite.display) this.appendCell(x, y, sprite.render());
        });
        return this;
    }

    addSprite(html, x, y, display) {
        let sprite = new Sprite(this, this.idSprite++, html, x, y, display);
        this.sprites.push(sprite);
        if(display) this.refreshCell(x, y);
        return sprite;
    }

    getSprite(sprite) {

        this.sprites = this.sprites.filter((s) => {
            if(s.html != sprite.html) return true;
            if(s.x != sprite.x) return true;
            if(s.y != sprite.y) return true;
            if(s.display != sprite.display) return true;
            return false;
        });

        return this.refreshCell(sprite.x, sprite.y);

    }

    deleteSprite(sprite, refresh = true) {
        this.sprites = this.sprites.filter((s) => ! s.is(sprite));
        if(!refresh) return;
        return this.refreshCell(sprite.x, sprite.y);
    }

    deleteSprites(x, y) {

        this.sprites = this.sprites.filter((s) => {
            if(s.x != x) return true;
            if(s.y != y) return true;
            return false;
        });

        return this.refreshCell(x, y);

    }

}

class Sprite {

    game;
    html;
    x;
    y;
    display = true;
    cellSharingRule = null;
    id = null;

    constructor(game, id, html, x, y, display = true) {
        this.game = game;
        this.html = html;
        this.x = x;
        this.y = y;
        this.display = display;
        this.id = id;
    }

    show() {
        this.display = true;
        this.game.refreshCell(this.x, this.y);
        return this;
    }

    hide() {
        this.display = false;
        this.game.refreshCell(this.x, this.y);
        return this;
    }

    onCollision(sprite) {
    }

    canMove(x, y) {

        if(!this.cellSharingRule)
            return true;
        
        let concurrentSprites = this.game.getCellSprites(x, y);

        if(!concurrentSprites.length)
            return true;

        return 0 == this.game.getCellSprites(x, y).filter((sprite) => !this.cellSharingRule(sprite)).length;
        
    }

    setCellSharingRule(callback) {
        this.cellSharingRule = callback;
        return this;
    }

    moveUp(step = 1) {
        if(this.y <= 0) return;
        let y = Math.max(0, this.y - step);
        while(!this.canMove(this.x, y) && y < this.y) y++;
        if(this.y == y) return;
        this.hide();
        this.y = y;
        this.show();
        return this;
    }

    moveDown(step = 1) {
        if(this.game.height <= this.y + 1) return;
        let y = Math.min(this.game.height, this.y + step);
        while(!this.canMove(this.x, y) && this.y < y) y--;
        if(this.y == y) return;
        this.hide();
        this.y = y;
        this.show();
        return this;
    }

    moveLeft(step = 1) {
        if(this.x <= 0) return;
        let x = Math.max(0, this.x - step);
        while(!this.canMove(x, this.y) && x < this.x) x++;
        if(this.x == x) return;
        this.hide();
        this.x = x;
        this.show();
        return this;
    }

    moveRight(step = 1) {
        if(this.game.width <= this.x + 1) return;
        let x = Math.min(this.game.width, this.x + step);
        while(!this.canMove(x, this.y) && this.x < x) x--;
        if(this.x == x) return;
        this.hide();
        this.x = x;
        this.show();
        return this;
    }

    moveTo(sprite, step = 1) {
        
        if(sprite.x == this.x && sprite.y == this.y) 
            return;

        if(sprite.x == this.x) 
            return sprite.y < this.y ? this.moveUp(Math.min(this.y - sprite.y, step)) : this.moveDown(Math.min(sprite.y - this.y, step));
        
        if(sprite.y == this.y) 
            return sprite.x < this.x ? this.moveLeft(Math.min(this.x - sprite.x, step)) : this.moveRight(Math.min(sprite.x - this.x, step));

        if(Math.random() < 0.5) 
            return sprite.y < this.y ? this.moveUp(Math.min(this.y - sprite.y, step)) : this.moveDown(Math.min(sprite.y - this.y, step));

        return sprite.x < this.x ? this.moveLeft(Math.min(this.x - sprite.x, step)) : this.moveRight(Math.min(sprite.x - this.x, step));

    }

    render() {
        return this.html;
    }

    hasCollisions() {
        return 1 < this.getConcurrentSprites().length;
    }

    getConcurrentSprites() {
        return this.game.getCellSprites(this.x, this.y).filter(s =>!this.is(s) && s.display == true);
    }

    is(sprite) {
        return sprite.html == this.html && sprite.display == this.display && sprite.x == this.x && sprite.y == this.y;
    }

}
